package com.restassuredday1;

import org.json.JSONException;
import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UpdateProductApi {

	public static void main(String[] args) throws JSONException {

		System.out.println("This is Sample Rest assured Api for Update service");

		RestAssured.baseURI = "http://142.93.219.181:3000/api";
		RequestSpecification request = RestAssured.given();

		request.header("Content-Type", "application/json");
		request.header("Authorization",
				"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYXN0X21vZGlmaWVkIjoxNTY0ODk5MjgxMjcyLCJleHBpcmVfYXQiOjE1NzAwODMyODEyNzMsInBheWxvYWQiOnsiaWQiOjIsIm5hbWUiOiJKYXRpbiBTaGFybWEiLCJlbWFpbCI6ImphdGludnNoYXJtYUBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJtb2JpbGVfbnVtYmVyIjoiMTIzMTIzMTIzMSJ9fQ.NI9SDyv0do3sArirr-XjZTxWODkYBIa3Zq3IY7OycPY");

		JSONObject requestParams = new JSONObject();
		requestParams.put("prod_name", "Pramod update Product");
		requestParams.put("prod_desc", "added via rest assured in the prodcut list");
		requestParams.put("prod_price", "2001");

		request.body(requestParams.toString());

		Response response = request.put("/v1/products/141");
		System.out.println(response.getStatusCode());
		System.out.println(response.asString());

	}

}
