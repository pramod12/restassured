package com.restassuredday1;

import org.json.JSONException;
import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class LoginApi {

	public static void main(String[] args) throws JSONException {
		// TODO Auto-generated method stub

		System.out.println("This is sample RestAssured Login API");
		// step1 : Enter the URI
		// Setup a Rest Request
		RestAssured.baseURI = "http://142.93.219.181:3000/user/";
		RequestSpecification request = RestAssured.given(); // Used when you are
															// making a Post or
															// Put request

		// Step 2 : Header -- > content/type -- application/Json

		request.header("Content-Type", "application/json");

		// Step:3 --Pass Request Body --> JSON
		JSONObject requestParams = new JSONObject();
		requestParams.put("email", "jatinvsharma@gmail.com");
		requestParams.put("password", "123123123");

		request.body(requestParams.toString());

		// Step4---> What type of request : Get/POST
		Response response = request.post("/signin");
		System.out.println(response.getStatusCode());
		System.out.println(response.asString());

	}

}
