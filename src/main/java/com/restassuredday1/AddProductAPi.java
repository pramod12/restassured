package com.restassuredday1;

import org.json.JSONException;
import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AddProductAPi {

	public static void main(String[] args) throws JSONException {
		// TODO Auto-generated method stub

		System.out.println("This is sample RestAssured for Add product API");
		// step1 : Enter the URI
		// Setup a Rest Request
		RestAssured.baseURI = "http://142.93.219.181:3000/api";
		RequestSpecification request = RestAssured.given(); // Used when you are
															// making a Post or
															// Put request

		// Step 2 : Header -- > content/type -- application/Json

		request.header("Content-Type", "application/json");
		request.header("Authorization",
				"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYXN0X21vZGlmaWVkIjoxNTY0ODk5MjgxMjcyLCJleHBpcmVfYXQiOjE1NzAwODMyODEyNzMsInBheWxvYWQiOnsiaWQiOjIsIm5hbWUiOiJKYXRpbiBTaGFybWEiLCJlbWFpbCI6ImphdGludnNoYXJtYUBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJtb2JpbGVfbnVtYmVyIjoiMTIzMTIzMTIzMSJ9fQ.NI9SDyv0do3sArirr-XjZTxWODkYBIa3Zq3IY7OycPY");

		// Step:3 --Pass Request Body --> JSON
		JSONObject requestParams = new JSONObject();
		requestParams.put("prod_name", "Pramod Product");
		requestParams.put("prod_desc", "added via rest assured in the prodcut list");
		requestParams.put("prod_price", "2000");

		request.body(requestParams.toString());

		// Step4---> What type of request : Get/POST
		Response response = request.post("/v1/products");
		System.out.println(response.getStatusCode());
		System.out.println(response.asString());

	}

}
