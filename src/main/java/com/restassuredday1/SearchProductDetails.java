package com.restassuredday1;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class SearchProductDetails {

	public static void main(String[] args) {

		System.out.println("This is Sample Rest assured Api for getproduct service");

		RestAssured.baseURI = "http://142.93.219.181:3000/api";
		RequestSpecification request = RestAssured.given();
		request.header("Authorization",
				"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYXN0X21vZGlmaWVkIjoxNTY0ODk5MjgxMjcyLCJleHBpcmVfYXQiOjE1NzAwODMyODEyNzMsInBheWxvYWQiOnsiaWQiOjIsIm5hbWUiOiJKYXRpbiBTaGFybWEiLCJlbWFpbCI6ImphdGludnNoYXJtYUBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJtb2JpbGVfbnVtYmVyIjoiMTIzMTIzMTIzMSJ9fQ.NI9SDyv0do3sArirr-XjZTxWODkYBIa3Zq3IY7OycPY");

		Response response = request.get("/v1/products/141");

		int statusCode = response.getStatusCode();
		String responseString = response.asString();
		System.out.println(statusCode);
		System.out.println(responseString);

	}

}
